<?php

// autoload_psr4.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'Goetas\\XML\\XSDReader\\' => array($vendorDir . '/goetas/xsd-reader/src'),
);
