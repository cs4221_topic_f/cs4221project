<?php

class Index extends Controller {

    function __construct() {
        parent::__construct();
    }
    
    function index() {
        $this->view->render('index/index', $data);
    }

    // ps. optional arg
    public function dummyFunction($arg = false) {
        echo 'We are inside dummy function<br>';
        
        if (isset($arg) && trim($arg) != '') {
            echo 'Optional: ' . $arg . '<br>';
        } 
    }
}

