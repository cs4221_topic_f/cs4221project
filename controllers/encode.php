<?php

class Encode extends Controller {    
    function __construct() {
        parent::__construct();
    }
    
    function index() {
        $data = array('encode');
        $this->view->render('encode/index', $data);
    }
    
    public function uploadFile($data = false) {
        $retData = array(); // data return to view        
        $retData[2] = true; // uploadFile is valid
        
        $xml = simplexml_load_file($_FILES['fileToUpload']['tmp_name']);      
        $xmlStr = $xml->asXML();
        
        // data[0] is xml2relation data array, $data[1] is xpath2sql data array
        // data[2] to check if valid, $data[3] to get the error message
                                
        $xml2Relation_data = $this->xml2Relation($xmlStr);
        //$retData[0] = $this->xml2Relation($xmlStr);
        // $retData[0] = $xml2Relation_data[0];
        // $retData[1] = $xml2Relation_data[1];
        // $retData[2] = $xml2Relation_data[2];

        //$retData[1] = $xml2Relation_data;
        $hasCreatedDB = $xml2Relation_data[0];
        
        if ($hasCreatedDB) {
            $retData[0] = $xml2Relation_data;
            $retData[1] = $xmlStr;
        } else {
            $retData[2] = false; // uploadFile is invalid
            $retData[3] = "Error in xml2relation";
        }
         
        echo json_encode($retData); // return the data to ajax (see public/js/main.js)
    }
    
    private function xml2Relation($xmlStr) {       
        require 'models/xml2relation_model.php';
        $xml2relation_model = new Xml2Relation_Model();
       
        $data = $xml2relation_model->xml2RelationDriver($xmlStr);
        return $data; 
    }    
    
    public function xPath2Sql() { 
        require 'models/xpath2sql_model.php';
        $xpath2sql_model = new XPath2Sql_Model();

        $xmlStr = $_POST["uploadedXml"];
        $xpathStr = $_POST["xpathStr"];
        $data = $xpath2sql_model->translateXPath($xmlStr, $xpathStr);

        echo json_encode($data);
    }
}

