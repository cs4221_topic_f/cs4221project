<?php
class Bootstrap {

    function __construct() {
        // URL: http://localhost/CS4221Project/controller/function/arg1/arg2/.../
        $url = isset($_GET['url']) ? strtolower($_GET['url']) : null;
        $url = rtrim($url, '/');
        $url = explode('/', $url);
        $path = 'controllers/' . $url[0] . '.php';
        
        if(file_exists($path)){
            require $path;
            $controller = new $url[0];
            $controller->loadModel($url[0]);
        } else {
            require 'controllers/index.php';
            $controller = new Index();
            $controller->index();
            return false;
        }

        // Calling methods
        $hasMethod = isset($url[1]) && method_exists($controller, $url[1]);

        if (isset($url[2]) && $hasMethod) { //if there is arg
            $controller->{$url[1]}($url[2]);
        } else if ($hasMethod) { // else if there is method
            $controller->{$url[1]}();
        } else {
            // The index page view of this controller is rendered only
            // if the url does not call a function
            // You will need to call render inside that function
            $controller->index();
        } 
    }
}