<?php

class View {

    function __construct() {
    }
    
    public function render($name, $data, $noInclude = false) {
        if ($noInclude) {
            require 'views/' . $name . '.php';
        } else {
            require 'views/header.php';        
            require 'views/' . $name . '.php';
            require 'views/footer.php';
        }
    }
}
