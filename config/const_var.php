<?php

define('ELEMENT_STR', 'ELEMENT');
define('ATTRIBUTE_STR', 'ATTRIBUTE');
define('TEXT_STR', 'TEXT');
define('DOCUMENT_STR', 'DOCUMENT');
define('OTHER_STR', 'OTHER');

define('TYPE_ATTR_ONLY', 'attr_only');
define('TYPE_COMPARE', 'compare');
define('TYPE_COMPLEX', 'complexType');
define('TYPE_SIMPLE', 'simpleType');
define('TYPE_SCHEMA', 'schema');
define('TYPE_SEQUENCE', 'sequence');
define('TYPE_SELECTOR', 'selector');
define('TYPE_FIELD', 'field');
define('TYPE_ANNOTATION', 'annotation');
define('TYPE_DOCUMENTATION', 'documentation');
define('TYPE_RESTRICTION', 'restriction');
define('TYPE_ATTRIBUTE', 'attribute');
define('TYPE_KEY', 'key');
define('TYPE_KEYREF', 'keyref');
define('TYPE_TEXT', '#text');

define('ATTR_NAME', 'name');
define('ATTR_TYPE', 'type');
define('ATTR_VALUE', 'value');
define('ATTR_BASE', 'base');
define('ATTR_USE', 'use');
define('ATTR_MINOCCURS', 'minOccurs');
define('ATTR_MAXOCCURS', 'maxOccurs');
define('ATTR_MAXLENGTH', 'maxLength');
define('NONE', 'none');

define('DOUBLE_SLASH', 'double_slash');
define('SINGLE_SLASH', 'single_slash');
define('NODE', 'node');

define('FINAL_SQL', 'WITH RECURSIVE descendants
  (nodeId, parentId, parentLabel, nodeLabel, openTag, content, nodetype) 
 AS ( SELECT nodeId, parentId, parentLabel, nodeLabel,openTag, content, nodetype, 0 AS level, cast((nodelabel || \'(id: \' || (nodeid) || \')\') as text) AS path, ARRAY[nodeid] AS npath
      FROM node
      WHERE nodeid in ({0}) 

      UNION ALL

      SELECT n.nodeId, n.parentId, n.parentLabel, n.nodeLabel,n.openTag, n.content, n.nodetype, level + 1, CAST(d.path || \' -> \' ||
      CAST((n.nodelabel || \'(id: \' || (n.nodeid) || \')\') AS text) AS text) AS path, npath || d.nodeid
      FROM node n
      JOIN descendants d ON d.nodeid  = n.parentid 
      AND d.nodeid <> n.nodeid
    ) 
SELECT d2.nodeid, d2.nodelabel, d2.path, d2.nodetype, d2.content 
FROM descendants d2
ORDER BY npath, level;');
