<?php

include 'models/xpath2sql_helper_model.php';
global $xpath2sql_helper_model;

class XPath2Sql_Model extends Model {

    function __construct() {
        parent::__construct();
    }

    public function translateXPath($xmlStr, $xpathStr) {
        global $xpath2sql_helper_model; // In order to access $xpath2sql_helper_model in each function, you need to declare this line first
        $xpath2sql_helper_model = new XPath2Sql_Helper_Model(); // to create db if it does not work, go to config -> database to change password       

        $data = array();
        $xml = new DOMDocument;
        $xml->loadXML($xmlStr);
        $xpathStr = strtolower($xpathStr);

        //TESTING---------------------------
//        $xpathStr = "root";
//        $xml->load("test_file\complex.xml");        
        // To test code, just echo or print_r
        // Then go to "localhost/cs4221/encode/xPath2Sql
        //TESTING--------------------------

        if ($xpath2sql_helper_model->validateXPath($xml, $xpathStr) === false) {
            $data[0] = false;
            $data[1] = 'Sorry, we are not able to process the given XPath.';
        } else {
            $xpath2sql_helper_model->processNXPath($xpathStr);
            $leftPathResult = $this->processLeftPath($xpathStr);
            $finalResult = str_replace('{0}', $leftPathResult, FINAL_SQL);
            $data[0] = SqlFormatter::highlight($finalResult);

            $results = $xpath2sql_helper_model->getResults($finalResult);
            $data[1] = $results[0]; //results array
            $data[2] = $results[1];
        }

        $data[3] = $xpathStr;
        return $data;
    }

    private function processLeftPath($nXpath) {
        global $xpath2sql_helper_model;
        $xpath2sql_helper_model = new XPath2Sql_Helper_Model();
        $wordArr = $xpath2sql_helper_model->splitXPathStr($nXpath);
        $length = sizeof($wordArr);
        $leftPath = '';
        $rootCheck = '';
        $attrCheck = '';

        $startNodeType = $xpath2sql_helper_model->getStartNodeType($wordArr);

        if ($startNodeType == DOUBLE_SLASH) {
            if ($length - 1 == 2) {
                $selection = 'nodeid';
                $whereSubject = $wordArr[2];
            } else {
                $selection = '*';
                $whereSubject = $wordArr[2];
            }

            if (strpos($whereSubject, '@') !== false) {
                $attrCheck = ' AND nodetype = \'Attribute\'';
            } else {
                $attrCheck = ' AND nodetype <> \'Attribute\'';
            }
            $startIndex = 2;
        } elseif ($startNodeType == SINGLE_SLASH) {
            if ($length - 1 == 1) {
                $selection = 'nodeid';
                $whereSubject = $wordArr[1];
            } else {
                $selection = '*';
                $whereSubject = $wordArr[1];
            }
            
            if (strpos($whereSubject, '@') !== false) {
                $attrCheck = ' AND nodetype = \'Attribute\'';
            } else {
                $attrCheck = ' AND nodetype <> \'Attribute\'';
            }

            $rootCheck = ' AND parentid IS NULL';
            $startIndex = 1;
            
        } elseif ($startNodeType == NODE) {
            if ($length == 1) {
                $selection = 'nodeid';
                $whereSubject = $wordArr[0];
            } else {
                $selection = '*';
                $whereSubject = $wordArr[0];
            }
            
            if (strpos($whereSubject, '@') !== false) {
                $attrCheck = ' AND nodetype = \'Attribute\'';
            } else {
                $attrCheck = ' AND nodetype <> \'Attribute\'';
            }
            
            $rootCheck = ' AND parentid IS NULL';
            $startIndex = 0;
        }

        $xpath2sql_helper_model->processAttribute($whereSubject);
        $leftPath .= 'SELECT ' . $selection . ' FROM node WHERE nodelabel = \'' . $whereSubject . '\'' . $rootCheck . $attrCheck;

        $counter = 0;
        for ($i = $startIndex; $i < $length - 1; $i++) {
            $parent = $wordArr[$i];
            $child = $wordArr[$i + 1];

            if ($child == '') {
                continue;
            } elseif ($parent == '') {
                $this->leftPath($wordArr[$i - 1], $child, $counter, $counter + 1, $leftPath, false);
            } else {
                $this->leftPath($parent, $child, $counter, $counter + 1, $leftPath, true);
            }

            if ($i + 1 == $length - 1) {
                $leftPath = 'SELECT DISTINCT n' . ($counter + 1) . '.nodeid FROM (' . $leftPath;
            } else {
                $leftPath = 'SELECT n' . ($counter + 1) . '.* FROM (' . $leftPath;
            }

            $counter += 2;
        }

        return $leftPath;
    }

    private function leftPath($parent, $child, $parentIndex, $childIndex, &$leftPath, $isAbsolute) {
        global $xpath2sql_helper_model;
        $xpath2sql_helper_model = new XPath2Sql_Helper_Model();

        $parentAlias = 'n' . $parentIndex;
        $childAlias = 'n' . $childIndex;
        $attrCheck = '';

        if (strpos($child, '@') !== false) {
            $attrCheck = ' AND ' . $childAlias . '.nodetype = \'Attribute\'';
            $isAttr = true;
        } else {
            $attrCheck = ' AND ' . $childAlias . '.nodetype <> \'Attribute\'';
            $isAttr = false;
        }

//        if (strpos($parent, '@') !== false) {
//            $attrCheck = ' AND ' . $parentAlias . '.typename = \'Attribute\'';
//        } else {
//            $attrCheck = ' AND ' . $parentAlias . '.typename <> \'Attribute\'';
//        }

        $xpath2sql_helper_model->processAttribute($parent);
        $xpath2sql_helper_model->processAttribute($child);

        if ($isAbsolute === true) {
            $leftPath .= ') ' . $parentAlias . ', node ' . $childAlias . ' WHERE ' .
                    $childAlias . '.parentid = ' . $parentAlias . '.nodeid AND ' .
                    $childAlias . '.nodelabel = \'' . $child . '\'' . $attrCheck;
        } else {
            if($isAttr === true) {
                $leftPath .= ') ' . $parentAlias . ', node ' . $childAlias . ' WHERE ' .
                    $childAlias . '.opentag >= ' . $parentAlias . '.opentag AND ' .
                    $childAlias . '.closetag <= ' . $parentAlias . '.closetag AND ' .
                    $childAlias . '.nodelabel = \'' . $child . '\'' . $attrCheck;
            } else {
                $leftPath .= ') ' . $parentAlias . ', node ' . $childAlias . ' WHERE ' .
                    $childAlias . '.opentag > ' . $parentAlias . '.opentag AND ' .
                    $childAlias . '.closetag < ' . $parentAlias . '.closetag AND ' .
                    $childAlias . '.nodelabel = \'' . $child . '\'' . $attrCheck;
            }   
        }

        $leftPath .= ' AND ' . $parentAlias . '.nodetype <> \'Attribute\'';
        
        return;
    }

    private function processPredicate(&$where, &$predicates, $parent, $child, $parentAlias, $childAlias) {
//        $parentPredicate = $predicates[$parent];
//        $childPredicate = $predicates[$child];
//        
//        if ($parentPredicate === false && $childPredicate === false) {
//            return;
//        }
//                
//        if ($parentPredicate !== false) {
//            $predicate = $parentPredicate;
//            
//        } else if ($childPredicate !== false) {
//            $predicate = $childPredicate;
//        } 
//        
//        $condition = $parentPredicate[KEY_CONDITION];
//        $predicateType = $parentPredicate[KEY_TYPE];
//        
//        if ($predicateType == TYPE_COMPARE) {
//            $where = ' WHERE ' . $parentAlias . '.' . $data[KEY_LHS] .
//                    ' ' . $data[KEY_OP] . ' ' . $parentAlias . '.' . $data[KEY_RHS] . ' ';
//        } else if ($predicate == TYPE_ATTR_ONLY){
//            
//        } else {
//            return;
//        }
    }

}
