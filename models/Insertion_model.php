<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Insertion_model
 *
 * @author CL
 */
class Insertion_model {
    private $nodeId;
    private $parentId;
    private $parentLabel;
    private $nodeLabel;
    private $nodeType;
    private $openTag;
    private $closeTag;
    private $content;
    
    public function __construct() {
    }
    
    
    public function getNodeId() {
        return $this->nodeId;
    }

    public function getParentId() {
        return $this->parentId;
    }

    public function getParentLabel() {
        return $this->parentLabel;
    }

    public function getNodeLabel() {
        return $this->nodeLabel;
    }

    public function getNodeType() {
        return $this->nodeType;
    }

    public function getOpenTag() {
        return $this->openTag;
    }

    public function getCloseTag() {
        return $this->closeTag;
    }

    public function getContent() {
        return $this->content;
    }

    public function setNodeId($nodeId) {
        $this->nodeId = $nodeId;
    }

    public function setParentId($parentId) {
        $this->parentId = $parentId;
    }

    public function setParentLabel($parentLabel) {
        $this->parentLabel = $parentLabel;
    }

    public function setNodeLabel($nodeLabel) {
        $this->nodeLabel = $nodeLabel;
    }

    public function setNodeType($nodeType) {
        $this->nodeType = $nodeType;
    }

    public function setOpenTag($openTag) {
        $this->openTag = $openTag;
    }

    public function setCloseTag($closeTag) {
        $this->closeTag = $closeTag;
    }

    public function setContent($content) {
        $this->content = $content;
    }


}
