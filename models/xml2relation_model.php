<?php
//include 'models/Insertion_model.php';
include ("Insertion_model.php");
global $debug;
global $insertions;
global $createSQL;
global $insertSQLs;
global $index;
global $tag;

$insertions = array();
$insertSQLs = array();
$index =0;
$tag = 0;
$debug = 0;

class Xml2Relation_Model extends Model {
    
    function __construct() {
        parent::__construct();
    }
    
    public function xml2RelationDriver($xmlStr){
        $debug=0;
        global $insertions;
        global $createSQL;
        global $insertSQLs;
        $data = array();
        if($debug)echo "file name: ".' '.$xmlStr."</br></br>";
        
        $xmlDoc = new DOMDocument();
        $xmlDoc->preserveWhiteSpace = false;
        $xmlDoc->loadXML($xmlStr);
        if($debug)echo "xmlDoc:" . "</br>";
        if($debug)var_dump($xmlDoc);
        if($debug)echo "</br></br>";

        $xml = $xmlDoc->documentElement;
        if($debug)echo "xml:" . "</br>";
        if($debug)var_dump($xml);
        if($debug)echo "</br></br>";

  
       
        $clearTable = "drop schema public cascade;create schema public;";
        $result = $this->db->exec($clearTable);
        
          // 2. create node Table in DB
        $this->createTable();
        
          // 3. map data to DB
         $rootParentId = "NULL";
        $this->createInsert($xml,$rootParentId);
        

        // 4. execute in DB
        $this->insertIntoTable();
        
        $data[0] = true;
        //$data[1] = $createSQL;      
        $data[1] = SqlFormatter::highlight($createSQL);      
        $data[2] = $insertSQLs;
        
         if ($debug) {
            foreach ($insertSQLs as $sql) {
                echo $sql . "</br>";
            }
        }
        
            
        return $data;
    }
    
    
   private function createTable() {
       $debug=0;
        
        global $createSQL;
        $sql = "CREATE TABLE node (nodeId int PRIMARY KEY, 
                parentId int,
                parentLabel text, 
                nodeLabel text, 
                nodeType text, 
                openTag int,
                closeTag int, 
                content text );";

        $createSQL = $sql;
        if($debug){
            echo "createSQL:"."</br>";
            echo $createSQL ."</br></br>";
        }
        
        $result = $this->db->exec($sql);
    }
    
    private function createInsert($node, $parentId){
        $debug =0;
        global $insertions;
        
        if($node->nodeType == XML_TEXT_NODE){
            return ;
        }
        
        $numOfAttr = $node->attributes->length;
        $numOfChildren = $node->childNodes->length;
        if($debug){
            echo "numOfAttr:".' '.$numOfAttr."</br>";
            echo "numOfChildren:".' '.$numOfChildren."</br>";
        }
        
        $nodeId = $this->getNodeId();
        $nodeLabel = $node->nodeName;
        $openTag = $this->getTagId();
        if ($node->parentNode->nodeType == 9) {
            $parentLabel = "NULL";
        } else {
            $parentLabel = $node->parentNode->nodeName;
        }
        if($debug){
            echo "nodeId: ".' '.$nodeId."</br>";
            echo "nodeLabel: ".' '.$nodeLabel."</br>";
            echo "openTag: ".' '.$openTag."</br>";
            echo "parentId: ".' '.$parentId."</br>";
            echo "parentLabel: ".' '.$parentLabel."</br>";
            echo "</br>";
        }
        
        $insertions[$nodeId] = new Insertion_model();
        $insertions[$nodeId]->setNodeId($nodeId);
        $insertions[$nodeId]->setNodeLabel($nodeLabel);
        $insertions[$nodeId]->setOpenTag($openTag);
        $insertions[$nodeId]->setParentId($parentId);
        $insertions[$nodeId]->setParentLabel($parentLabel);
      
        
//        // insert each attribute node
        if ($numOfAttr > 0) {
            foreach ($node->attributes as $attr) {
                $id = $this->getNodeId();
                $label = $attr->name;
                $pLabel = $nodeLabel;
                $pId = $nodeId;
                $type = "Attribute";
                $oTag = $openTag;
                $cTag = $openTag;
                $attrContent = $attr->value;
                $attrContent = str_replace("'", "''", $attrContent);

                $insertions[$id] = new Insertion_model();
                $insertions[$id]->setNodeId($id);
                $insertions[$id]->setNodeLabel($label);
                $insertions[$id]->setNodeType($type);
                $insertions[$id]->setParentId($pId);
                $insertions[$id]->setParentLabel($pLabel);
                $insertions[$id]->setOpenTag($oTag);
                $insertions[$id]->setCloseTag($cTag);
                $insertions[$id]->setContent($attrContent);
                
                if($debug){
                    echo "attributes"."</br>";
                    print_r($insertions[$id]);
                    echo "</br></br>";
                }
            }
        }
        
        if ($numOfChildren > 0) {
            if ($numOfChildren == 1 && $node->firstChild->nodeType == 3) {
                $insertions[$nodeId]->setNodeType("Text");
                $insertions[$nodeId]->setContent(str_replace("'", "''", $node->nodeValue));
                $insertions[$nodeId]->setCloseTag($this->getTagId());
            } else {
                //$insertions[$nodeId]->setContent("null");
                $tempContent = "";
                foreach ($node->childNodes as $child) {
                    if ($child->nodeType == 3) {
                        $tempContent .= $child->wholeText;
                    }
                    $this->createInsert($child, $nodeId);
                }
                $insertions[$nodeId]->setNodeType("Element");
                $insertions[$nodeId]->setCloseTag($this->getTagId());
                $insertions[$nodeId]->setContent(str_replace("'", "''", $tempContent));
            }
        }
    }
    
    private function insertIntoTable() {
        global $insertions;
        global $insertSQLs;

        foreach ($insertions as $insert) {
            $nodeId = $insert->getNodeId();
            $parentId = $insert->getParentId();
            $parentLabel = $insert->getParentLabel();
            $nodeLabel = $insert->getNodeLabel();
            $nodeType = $insert->getNodeType();
            $openTag = $insert->getOpenTag();
            $closeTag = $insert->getCloseTag();
            $content = $insert->getContent();

            $sql = "INSERT INTO node VALUES ($nodeId, $parentId, '$parentLabel', '$nodeLabel', '$nodeType', $openTag, $closeTag, '$content');";

            //$insertSQLs[] = $sql;
            $insertSQLs[] = SqlFormatter::highlight($sql);
            $result = $this->db->exec($sql);
        }
 
    }


    private function getNodeId() {
        global $index;
        $index = $index + 1;
        return $index;
    }

    private function getTagId() {
        global $tag;
        $tag = $tag + 1;
        return $tag;
    }

    /*
    //XSD to Schema
    var $xmlReader;

    function initializeXmlReader() {
        $xmlReader = new XMLREADER();
    }

    function closeXmlReader() {
        if ($xmlReader != null) {
            $xmlReader.close();
        }
    }

    function loadXsd($xsdPath) {
        if ($xmlReader == null) {
            initializeXmlReader();
        }

        $xmlReader->open($xsdPath);

        return $xmlReader;
    }

    function readXsd($xmlReader) {
        while($xmlReader->read()) {
            if ($t->localName == ELEMENT_STR) {
                if ($t->nodeType != 15) {
                    $eleName = $t->getAttribute(ATTR_NAME);
                    $eleType = substr($t->getAttribute(ATTR_TYPE), 3);
                    if ($eleType == "") {
                        $eleType = NONE;
                    }
                    $dataType[$eleName] = $eleType;
                }  
            } elseif ($t->localName == TYPE_COMPLEX) {
                if (end($dataType) == NONE) {
                    $lastKey = end(array_keys($dataType));
                    reset($dataType);
                    $dataType[$lastKey] = $t->localName;
                }
            } elseif ($t->localName == TYPE_SIMPLE) {
                echo "Simple tag, name: ".$t->getAttribute(ATTR_NAME)."<br/>";
                if (end($dataType) == NONE) {
                    $lastKey = end(array_keys($dataType));
                    reset($dataType);
                    $dataType[$lastKey] = $t->localName;
                }

                $t->read(); $t->read();
                echo '&emsp;'.$t->localName."<br/>";
                if ($t->localName == TYPE_ANNOTATION) {
                    echo '&emsp;'.'&emsp;'.$t->readOuterXML()."<br/>";
                }

                $t->read(); $t->read();
                echo '&emsp;'.$t->localName."<br/>";
                if ($t->localName == TYPE_DOCUMENTATION) {
                    echo '&emsp;'.'&emsp;'.$t->readOuterXML()."<br/>";
                }

                $t->read(); $t->read(); $t->read(); $t->read(); $t->read(); $t->read(); 
                echo '&emsp;'.$t->localName."<br/>";
                if ($t->localName == TYPE_RESTRICTION) {
                    echo '&emsp;'.'&emsp;'.substr($t->getAttribute(ATTR_BASE), 3)."<br/>";
                }

                $t->read(); $t->read();
                echo '&emsp;'.$t->localName."<br/>";
                if ($t->localName == ATTR_MAXLENGTH) {
                    echo '&emsp;'.'&emsp;'.$t->getAttribute(ATTR_VALUE)."<br/>";
                }


            } elseif ($t->localName == TYPE_ATTRIBUTE) {
                echo "Attribute name: ".$t->getAttribute(ATTR_NAME).", Type: ".$t->getAttribute(ATTR_TYPE).", Use: ".$t->getAttribute(ATTR_USE)."<br/>";
            } elseif ($t->localName == TYPE_KEY) {
                echo "Key name: ".$t->getAttribute(ATTR_NAME)."<br/>";
            } elseif ($t->localName == TYPE_KEYREF) {
                echo "KeyRef name: ".$t->getAttribute(ATTR_NAME)."<br/>";
            } elseif ($t->localName == TYPE_RESTRICTION) {
                echo "Restriction base: ".substr($t->getAttribute(ATTR_BASE), 3)."<br/>";
            } elseif ($t->localName == ATTR_MAXLENGTH) {
                echo "Max length value: ".$t->getAttribute(ATTR_VALUE)."<br/>";
            } elseif ($t->localName == "choice") {
                echo "Min occurs: ".$t->getAttribute(ATTR_MINOCCURS).", Max occurs: ".$t->getAttribute(ATTR_MAXOCCURS)."<br/>";
            } elseif ($t->localName == TYPE_TEXT || $t->localName == TYPE_SCHEMA || $t->localName == TYPE_SEQUENCE || $t->localName == TYPE_SELECTOR || $t->localName == TYPE_FIELD || $t->localName == TYPE_ANNOTATION || $t->localName == TYPE_DOCUMENTATION) {
            //echo $t->localName . " ignored <br/>"; //#text, schema, sequence ignored. <br/>";
            } else {
                echo "THIS IS NOT HANDLED: ".$t->localName."<br/>";
            }
        }

        print_r($dataType);

        $this->closeXmlReader();
    }

    function isIgnoredTag($element) {
        if ( strcasecmp($element, TYPE_TEXT) == 0 || 
            strcasecmp($element, TYPE_SCHEMA) == 0 || 
            strcasecmp($element, TYPE_SEQUENCE) == 0 || 
            strcasecmp($element, TYPE_SELECTOR) == 0 || 
            strcasecmp($element, TYPE_FIELD) == 0 || 
            strcasecmp($element, TYPE_ANNOTATION) == 0 || 
            strcasecmp($element, TYPE_DOCUMENTATION) == 0) {
            return true;
        } else {
            return false;
        }
    }
    */
}
