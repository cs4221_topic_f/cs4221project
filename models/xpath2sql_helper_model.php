<?php

class XPath2Sql_Helper_Model extends Model {

    function __construct() {
        parent::__construct();
    }

    public function getResults($query) {
        $sth = $this->db->query($query);
        $columnData = $sth->fetch(PDO::FETCH_ASSOC);
        $columns = array();
        foreach ($columnData as $field => $value) {
            array_push($columns, $field);
        }
        $data[1] = $columns;
        
        $sth = $this->db->query($query);
        $data[0] = $sth->fetchAll();        
        
        return $data;
    }
    
    // return false if badly formed
    // else return the attribute without @
    public function parseAttribute($xpathPart) {
        $posAttr = strpos($xpathPart, '@');
        $countAttr = substr_count($xpathPart, '@');

        if ($posAttr == 0 && $countAttr == 1) {
            $data = substr($xpathPart, 1);
        } else {
            return false;
        }

        return $data;
    }

    public function splitXPathStr($xpathStr) {
        $xpathArr = explode("/", $xpathStr);

        return $xpathArr;
    }
    
    public function getStartNodeType($wordArr) {
        if ($wordArr[0] == '' && $wordArr[1] == '') {
            return DOUBLE_SLASH;
        } elseif($wordArr[0] == '' && $wordArr[1] != '') {
            return SINGLE_SLASH;
        } else {
            return NODE;
        }
    }

    public function processNXPath(&$nXpath) {
        $nXpath = preg_replace('/\[.*?\]/', '', $nXpath);
    }
    
    public function processAttribute(&$word) {
        $word = preg_replace('/[@]/', '', $word);
    }

    public function validateXPath($xml, $xpathStr) {
        $xpath = new DOMXPath($xml);
        $resultNodes = $xpath->evaluate($xpathStr);

        if ($resultNodes === false) {
            
            return false;
        } else {            
            // check if there is predicate
            if($this->hasPredicate($xpathStr)){
                return false;
            }
        }
        
        $data[0] = $resultNodes;
                
        return $data;
    }
    
    // Does not mean it is valid syntax
    private function hasPredicate($xpathPart) {        
        $posClose = strpos($xpathPart, ']');
        $posOpen = strpos($xpathPart, '[');

        return ($posClose !== false && posOpen !== false);
    }
}
