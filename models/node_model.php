<?php
   class Node_Model {
      /* Member variables */
      private $nodeValue = "";
      private $nodeType = "";
      
      public function __construct() {
          
      }
      
//      public function createNode(){
//          $obj = new self();
//          return $obj;
//      }
      
//      public function createNodeWithValueAndType($nValue, $nType){
//          $obj = new self();
//          $this->nodeValue = $nValue;
//          $this->nodeType = $nType;
//          return $obj;
//      }
      
      /* Member functions */
      public function setNodeValue($nValue){
         $this->nodeValue = $nValue;
      }
      
      public function setNodeType($nType){
         $this->nodeType = $nType;
      }
      
      public function setNodeValueAndType($nValue,$nType){
         $this->nodeValue = $nValue;
         $this->nodeType = $nType;
      }
      
      public function getNodeValue(){
         return $this->nodeValue;
      }
      
      public function getNodeType(){
         return $this->nodeType;
      }
   }
?>