$(document).ready(function () {
    var uploadedXml;

    //--------------------------------------------------------------------------------------------------------------
    // File Upload AJAX
    //--------------------------------------------------------------------------------------------------------------
    $("#upload-form").on('submit', (function (e) {
        e.preventDefault();
        $.ajax({
            url: "/cs4221project/encode/uploadfile", // Url to which the request is send
            type: "POST", // Type of request to be send, called as method
            data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
            contentType: false, // The content type used when sending data to the server.
            cache: false, // To unable request pages to be cached
            processData: false, // To send DOMDocument or non processed data file it is set to false
            dataType: 'json',
            beforeSend: function () {
                showLoadingDiv(0);
            },
            xhr: function () {
                var myXhr = $.ajaxSettings.xhr();
                if (myXhr.upload) {
                    myXhr.upload.addEventListener('progress', showProgress, false);
                } else {
                }
                return myXhr;
            },
            success: function (data)   // A function to be called if request succeeds
            {
                // data is the $retData from encode.php
                // data[0] is xml2relation data array, $data[1] is xpath2sql data array
                // data[2] to check if valid, $data[3] to get the error message
                if (data[2] == true) {
                    renderXML2RelationTranslate(data);
                } else {

                }
                
                uploadedXml = data[1];

                setTimeout(function () {
                    hideLoadingDiv(0);

                    setTimeout(function () {
                        showFunctionDivs();
                        
                        $('html, body').animate({
                            scrollTop: $("#upload-btn").offset().top
                        }, 800);
                        $('#xml2relation-container').focus();
                    }, 400);
                }, 200);

                $('.disabled-panel').addClass('disabled-button');
                $('#xpath-str-input').val("");
                $('#xpath-str-input').focus();
            }
        })
    }));

    var fileTypes = ['xml'];
    $(function () {
        $("#fileToUpload").change(function () {
            if (this.files && this.files[0]) {
                var extension = this.files[0].name.split('.').pop().toLowerCase(), //file extension from input file
                        isSuccess = fileTypes.indexOf(extension) > -1;  //is extension in acceptable types

                if (isSuccess) { //yes
                    var reader = new FileReader();
                    reader.onload = function () {
                        $('.disabled-panel').removeClass('disabled-button');
                    }
                    $('#file-path').text(this.files[0].name);
                    reader.readAsDataURL(this.files[0]);
                } else { //no
                    hideFunctionDivs();
                }
            }
        });
    });

    $(function () {
        $("#fileToUpload").click(function () {
            $(this).val("");
            $("#file-path").text("No file selected");
            $('.disabled-panel').addClass('disabled-button');

            hideFunctionDivs();
        });
    });

    //--------------------------------------------------------------------------------------------------------------
    // XPath Syntax textbox validators
    //--------------------------------------------------------------------------------------------------------------
    initXPath2SqlValidators();

    //--------------------------------------------------------------------------------------------------------------
    // XPath Syntax Form AJAX
    //--------------------------------------------------------------------------------------------------------------
    $('#xpath-form').on("submit", function (event) {
        var xpathStr = $.trim($('#xpath-str-input').val());
        event.preventDefault();
        $('#xpath-form-btn').blur();
        
        if (xpathStr != '') {
            $.ajax({
                url: "/cs4221project/encode/xpath2sql",
                type: "POST",
                data: {uploadedXml: uploadedXml, xpathStr: xpathStr},
                dataType: 'json',
                beforeSend: function () {
                    showLoadingDiv(1);
                },
                xhr: function () {
                    var myXhr = $.ajaxSettings.xhr();
                    if (myXhr.upload) {
                        myXhr.upload.addEventListener('progress', showProgress2, false);
                    } else {
                    }
                    return myXhr;
                },
                success: function (data)
                {
                    console.log(data);
                    if (data[0] == false) {
                        $('#xpath-str-input').text("");
                        renderTranslateError(data[1]);
                    } else {
                        removeTranslateError();
                        renderXPathTranslate(data);

                        $('html, body').animate({
                            scrollTop: $("#xpath-form-btn").offset().top
                        }, 2000);
                        $('#translation-result').focus();
                    }
                    
                    hideLoadingDiv(1);                    
                }
            })
        }
    });
});

function renderXML2RelationTranslate(data) {    
    $('#x2r-partial-load').fadeOut("200", function () {
        $('#x2r-partial-load').load('./views/xml2relation/index.php', function () {
            // Put result data into respective divs
            renderX2RResultsTable(data);

            $('#x2r-partial-load').fadeIn('200');
        });
    });
}

function renderTranslateError(errorMsg) {
    var controlGroup = $('#xpath-form .xpath-cg');
    controlGroup.addClass('validation error');
    
    $('<p class="xpath-error-tip tip error ">' + errorMsg + '</p>').insertAfter('#xpath-str-input');
}

function removeTranslateError() {
    var controlGroup = $('#xpath-form .xpath-cg');
    controlGroup.removeClass('validation error');
    
    var errorTip = $('.xpath-error-tip');
    errorTip.remove();
}

function renderXPathTranslate(data) {    
    $('#partial-load').fadeOut("200", function () {
        $('#partial-load').load('./views/xpath2sql/index.php', function () {
            // Put result data into respective divs
            if (data[0] != false) {
                $("#translation-result").append(data[0]);
                
                // render modal
                $('#xpath-string').append(data[3]);
                renderResultsTable(data);                
            } else {
                // failed to process
            }
            $('#partial-load').fadeIn('200');
        });
    });
}

//function renderXML2Relation(data){
//    var isValidXML = data[0];
//    var msg;
//    var createSQL;
//    var insertSQL;
//    
//    
//    if( !isValidXML){
//         msg = data[1]; 
//    }else{
//         createSQL = data[1];
//         insertSQL = data[2];
//    } 
//}

function renderX2RResultsTable(data) {
    var xmlData = data[0];
    var createStatement = xmlData[1];
    var insertStatements = xmlData[2];

    var printSQLStatements = $('#print-xml-table');
    
    //Create Statement
    var trimmedCreateStatement = createStatement.replace(/\s\s+/g, ' ');
    printSQLStatements.append(createStatement);

    //Insert Statements
    for (i = 0; i < insertStatements.length; i++) {
        var trimmedInsertStatement = insertStatements[i].replace(/\s\s+/g, ' ');
        printSQLStatements.append(trimmedInsertStatement);

        hasResults = true;
    }
    
    if (hasResults != true) {
        $('#view-results-btn').addClass('disabled-button');
        renderTranslateError('Opps! no nodes were retrieved.');
    } else {
        $('#view-results-btn').removeClass('disabled-button');
    }
}

function renderResultsTable(data) {
    var results = data[1];
    var columns = data[2];
    var hasResults = false;
    var tr = $('#results-table thead tr');

    for (i = 0; i < columns.length; i++) {
        tr.append('<th class="align-left">' + columns[i] + '</th>');
    }

    var tbody = $('#results-table tbody');
    var row;
    for (i = 0; i < results.length; i++) {
        row = results[i];
        tbody.append('<tr>');
        for (j = 0; j < columns.length; j++) {
            tbody.append('<td data-title="' + columns[j] + '" class="align-left">' + row[j] + '</td>');
        }
        tbody.append('</tr>');

        hasResults = true;
    }
    
    if (hasResults != true) {
        $('#view-results-btn').addClass('disabled-button');
        renderTranslateError('Opps! no nodes were retrieved.');
    } else {
        $('#view-results-btn').removeClass('disabled-button');
    }
}


function initXPath2SqlValidators() {
    if ($("div:first").is("#encode-container")) {
        Ink.requireModules(['Ink.UI.FormValidator_1', 'Ink.Dom.Event_1'], function (FormValidator, InkEvent) {
            var myForm = Ink.i('xpath-form');
            var myInput = Ink.i('xpath-str-input');

            InkEvent.on(myInput, 'keyup', function (event) {
                var isValid = FormValidator.validate(myForm);
            });

            InkEvent.on(myForm, 'submit', function (event) {
                var isValid = FormValidator.validate(myForm);
            });
        });
    }
}

function setProgress(percent, $element) {
    var progressBarWidth = percent * $element.width();
    $element.find('div').animate({width: progressBarWidth}, 1000);    
}

function showProgress(event) {
    if (event.lengthComputable) {
        var uploaded = (event.loaded / event.total);
        setProgress(uploaded, $('#upload-progress-bar'));
    }
}

function showProgress2(event) {
    if (event.lengthComputable) {
        var uploaded = (event.loaded / event.total);
        setProgress(uploaded, $('#xpath-progress-bar'));
    }
}

function showLoadingDiv($that) {
    if ($that == 0) {
        $('#upload-progress-bar').fadeIn("200", function () {
        });
    } else {        
        $('#partial-load').html("");
        $('#xpath-progress-bar').fadeIn("200", function () {
        });
    }

}

function hideLoadingDiv($that) {
    if ($that == 0) {
        $('#upload-progress-bar').fadeOut("200", function () {
            $('#upload-progress-bar div').width(0);
        });
    } else {
        $('#xpath-progress-bar').fadeOut("200", function () {
            $('#xpath-progress-bar div').width(0);
        });
    }
}

function showFunctionDivs() {
    $('#xml2relation-container').fadeIn("400", function () {
    });
    $('#xpath2sql-container').fadeIn("400", function () {
    });
}

function hideFunctionDivs() {
    $('#xml2relation-container').fadeOut("400", function () {
    });
    $('#xpath2sql-container').fadeOut("400", function () {
    });
}
