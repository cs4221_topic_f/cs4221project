<div id="xpath2sql-result-container">
    <!--    <div class="panel-header backdrop">
            <strong class="panel-text"></strong>
        </div>-->
    <div class="panel-body backdrop">
        <div id="translation-result" class="medium panel-text wordwrap"></div>
    </div>

    <div class="column-group all-20 medium-100 small-100 tiny-100 push-right margin-up-10">
        <div class="control">
            <a href="#" id="view-results-btn" class="ink-button button-margin" type="submit"><span class="fa fa-table"></span> View Results</a>
        </div>  
    </div>  


    <div class="ink-shade fade">
        <div id="results-modal" class="ink-modal fade" data-trigger="#view-results-btn" data-width="80%" data-height="80%" role="dialog" aria-hidden="true" aria-labelled-by="modal-title">
            <div class="modal-header">
                <button class="modal-close ink-dismiss"></button>
                <h4 id="modal-title">Results</h4>
                <div id="xpath-string"></div>
            </div>
            <div class="modal-body" id="modalContent">
                <table id='results-table' class="ink-table bordered">
                    <thead>
                        <tr>                            
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <script>
        // Not required if you are using autoload.js.
        Ink.requireModules(['Ink.Dom.Selector_1', 'Ink.UI.Modal_1'], function (Selector, Modal) {
            var modalElement = Ink.s('#results-modal');
            var modalObj = new Modal(modalElement);
        });
    </script>
</div>



