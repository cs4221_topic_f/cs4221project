<?php
    //Set no caching
    header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
    header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); 
    header("Cache-Control: no-store, no-cache, must-revalidate"); 
    header("Cache-Control: post-check=0, pre-check=0", false);
    header("Pragma: no-cache");
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>CS4221 Project - Topic F</title>
        <!----------------------------------------------------------------------------------------------------------------------->
        <!----------------------------------------------------------------------------------------------------------------------->
        <link rel="stylesheet" type="text/css" href="http://fastly.ink.sapo.pt/3.1.10/css/ink.css"> <!-- inks css file -->   
        <link rel="stylesheet" type="text/css" href="/cs4221project/libs/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="/cs4221project/public/css/main.css"> <!-- main css file -->   
        <!----------------------------------------------------------------------------------------------------------------------->
        <!----------------------------------------------------------------------------------------------------------------------->
        <script src="//code.jquery.com/jquery-1.12.0.min.js"></script> <!-- jquery -->
        <script type="text/javascript" src="http://fastly.ink.sapo.pt/3.1.10/js/ink-all.js"></script> <!-- inks js bundle -->
        <script type="text/javascript" src="http://fastly.ink.sapo.pt/3.1.10/js/autoload.js"></script> <!-- inks js bundle -->
        <script type="text/javascript" src="/cs4221project/public/js/main.js"></script> <!-- main js file -->        
    </head>
    <body>
    <!----------------------------------------------------------------------------------------------------------------------->
    <!----------------------------------------------------------------------------------------------------------------------->    
    <?php 
    ?>
    <!----------------------------------------------------------------------------------------------------------------------->
    <!----------------------------------------------------------------------------------------------------------------------->     
    <nav class="ink-navigation">
        <ul class="menu horizontal black">
            <li class="heading>"><a href="<?php echo BASE_URL ?>">CS4221 Project - Topic F</a></li>
            <li class="nav-encode <?php echo ($data[0] == 'encode') ? 'active' : '' ?>"><a href="<?php echo BASE_URL ?>encode">Encode</a></li>
        </ul>
    </nav>
    <br>
     
    