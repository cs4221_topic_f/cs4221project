<div class="ink-grid">
    <div class="column-group horizontal-gutters">
        <div class="all-100">
            <h2>2.6 Topic F: Encoding XML into relations, and XPath into SQL.<br>
                The goal of this project is to implement two functions:</h2>
            <p>Contact: Pierre Senellart: pierre.senellart@nus.edu.sg</p>
        </div>
    </div>
    <div class="column-group horizontal-gutters">
        <div class="all-50">
            <p>A function <span class="ink-label blue">XML2Relation</span> that transforms an arbitrary XML document d provided as input
                into a sequence of SQL CREATE and INSERT statements that creates a database D formed of
                one or multiple relations encoding the content of the XML document as a relation (you can
                use the encoding of your choice but no assumption can be made on the XML document as
                input)</p>
        </div>
        <div class="all-50">
            <p>A function <span class="ink-label yellow">XPath2SQL</span> that transforms a simple XPath expression q provided as input into a
                SQL SELECT statement Q that encodes this query (to simplify, you can assume that the XPath
                expression has a very simple form; at minimum, you need to support / and // navigation on
                the child and descendant axes).</p>
        </div>
    </div>
    <div class="column-group horizontal-gutters">
        <div class="all-100">
            <p>The result of evaluating Q over D should be identical to the result of evaluating q over d.</p>
        </div>
    </div>
</div>
