<div id="encode-container">
    <div class="ink-grid">
        <form id="upload-form" class="ink-form" action="" method="post" enctype="multipart/form-data">
            <div><label for="upload-xml"><strong>1. XML File Upload</strong></label></div>     
            <p class="tip margin-bottom-10">
                Select a XML file to upload:
            </p>
            <div class="control-group">
                <div class="control">
                    <div class="control all-100 open-dialog-ctrl">
                        <input class="input-ctrl" type="file" name="fileToUpload" id="fileToUpload" required="">
                        <label class="margin-right-10" for="fileToUpload">Choose a XML file</label>
                        <span  id="file-path" class="tip">No file selected</span>
                    </div>             
                    <div class="column-group all-20 medium-100 small-100 tiny-100 push-right disabled-panel disabled-button">
                        <button id="upload-btn" class="child-fill ink-button button-margin" type="submit" 
                        value="Upload Image" name="uploadSubmit">
                        <span class="fa fa-upload"></span> Upload
                    </button>
                </div>    
            </div>                
        </div>     
    </form>
    <div id="upload-progress-bar" class="control all-100 hide"><div></div></div>
</div>    
</div>


<div>
    <div id="xml2relation-container" class="ink-grid hide">
        <hr>
        <div><label for="x2r-str"><strong>2. XML Document to relational database</strong></label></div>
        <form id="copyx2r-form" class="ink-form">
            <div class="panel-body backdrop">
                <div id="print-xml-table" class="max-height purewordwrap">
                </div>
            </div>  
        </form>
        <div id="x2r-partial-load">
        </div>
    </div>
</div>


<div>
    <div id="xpath2sql-container" class="ink-grid hide">
        <hr>
        
        <form id="xpath-form" class="ink-form">                        
            <div><label for="xpath-str"><strong>3. XPath Syntax</strong></label></div>     
            <p class="tip margin-bottom-10">
                Type in the XPath in the textbox below to start the parsing. We will parse your XPath string into SQL query.<br>
            </p>
            <div class="xpath-cg control-group">
                <div class="control">
                    <div class="control all-100">
                        <input class="ink-fv-required" id="xpath-str-input" name="xpath-str" type="text">
                    </div>                    
                    <div class="column-group all-20 medium-100 small-100 tiny-100 push-right">
                        <div class="control xpath-control">
                            <button id="xpath-form-btn" class="ink-button child-fill button-margin" type="submit"><span class="fa fa-file-code-o"></span> Parse</button>
                        </div>  
                    </div>         
                </div>                
            </div>            
        </form>
        <div id="xpath-progress-bar" class="control all-100 hide"><div></div></div>
        <div id="partial-load">

        </div>
    </div>
</div>