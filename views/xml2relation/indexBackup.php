<?php
        $xsdstring = "http://localhost/cs4221project/test_file/xsdSample01.xsd";
        $t = new XMLREADER();
        $t->open($xsdstring);

        $dataType = array();

        while($t->read()) {
            // if ( isIgnoredTag($t->localName) == false) {
            //     echo $t->localName." ".$t->getAttribute('name')." is ";
            //     if ($t->nodeType == 1) {
            //         echo "Start tag with ";
            //     } else if ($t->nodeType == 15) {
            //         echo "End tag with ";
            //     }
            // }

            if ($t->localName == "element") {
                // echo "".
                // ($t->getAttribute('type') == "" ? '' : "type: ".substr($t->getAttribute('type'), 3));

                // ($t->getAttribute('minOccurs')!='' ? "minOccurs: ".$t->getAttribute('minOccurs') : ", minOccurs: -");

                // ($t->getAttribute('maxOccurs')!='' ? ", maxOccurs: ".$t->getAttribute('maxOccurs') : ", maxOccurs: -");    

                // ($t->getAttribute('use')!='' ? ", use: ".$t->getAttribute('use')."<br/>" : ", use: -"."<br/>");

                // echo "<br/>";
                if ($t->nodeType != 15) {
                    $eleName = $t->getAttribute(ATTR_NAME);
                    $eleType = substr($t->getAttribute(ATTR_TYPE), 3);
                    if ($eleType == "") {
                        $eleType = NONE;
                    }
                    $dataType[$eleName] = $eleType;
                }  
            } elseif ($t->localName == "ComplexType") {
                if (end($dataType) == NONE) {
                    $lastKey = end(array_keys($dataType));
                    reset($dataType);
                    $dataType[$lastKey] = $t->localName;
                }
            } elseif ($t->localName == TYPE_SIMPLE) {
                echo "Simple tag, name: ".$t->getAttribute(ATTR_NAME)."<br/>";
                if (end($dataType) == NONE) {
                    $lastKey = end(array_keys($dataType));
                    reset($dataType);
                    $dataType[$lastKey] = $t->localName;
                }

                $t->read(); $t->read();
                echo '&emsp;'.$t->localName."<br/>";
                if ($t->localName == TYPE_ANNOTATION) {
                    echo '&emsp;'.'&emsp;'.$t->readOuterXML()."<br/>";
                }

                $t->read(); $t->read();
                echo '&emsp;'.$t->localName."<br/>";
                if ($t->localName == TYPE_DOCUMENTATION) {
                    echo '&emsp;'.'&emsp;'.$t->readOuterXML()."<br/>";
                }

                $t->read(); $t->read(); $t->read(); $t->read(); $t->read(); $t->read(); 
                echo '&emsp;'.$t->localName."<br/>";
                if ($t->localName == TYPE_RESTRICTION) {
                    echo '&emsp;'.'&emsp;'.substr($t->getAttribute(ATTR_BASE), 3)."<br/>";
                }

                $t->read(); $t->read();
                echo '&emsp;'.$t->localName."<br/>";
                if ($t->localName == ATTR_MAXLENGTH) {
                    echo '&emsp;'.'&emsp;'.$t->getAttribute(ATTR_VALUE)."<br/>";
                }


            } elseif ($t->localName == TYPE_ATTRIBUTE) {
                echo "Attribute name: ".$t->getAttribute(ATTR_NAME).", Type: ".$t->getAttribute(ATTR_TYPE).", Use: ".$t->getAttribute(ATTR_USE)."<br/>";
            } elseif ($t->localName == TYPE_KEY) {
                echo "Key name: ".$t->getAttribute(ATTR_NAME)."<br/>";
            } elseif ($t->localName == TYPE_KEYREF) {
                echo "KeyRef name: ".$t->getAttribute(ATTR_NAME)."<br/>";
            } elseif ($t->localName == TYPE_RESTRICTION) {
                echo "Restriction base: ".substr($t->getAttribute(ATTR_BASE), 3)."<br/>";
            } elseif ($t->localName == ATTR_MAXLENGTH) {
                echo "Max length value: ".$t->getAttribute(ATTR_VALUE)."<br/>";
            } elseif ($t->localName == "choice") {
                echo "Min occurs: ".$t->getAttribute(ATTR_MINOCCURS).", Max occurs: ".$t->getAttribute(ATTR_MAXOCCURS)."<br/>";
            } elseif ($t->localName == TYPE_TEXT || $t->localName == TYPE_SCHEMA || $t->localName == TYPE_SEQUENCE || $t->localName == TYPE_SELECTOR || $t->localName == TYPE_FIELD || $t->localName == TYPE_ANNOTATION || $t->localName == TYPE_DOCUMENTATION) {
            //echo $t->localName . " ignored <br/>"; //#text, schema, sequence ignored. <br/>";
            } else {
                echo "THIS IS NOT HANDLED: ".$t->localName."<br/>";
            }

            //echo "<br/>";
            //print_r($dataType);
            //echo "<br/>";
        }

        print_r($dataType);

        $t->close();

        function isIgnoredTag($element) {
            if ( strcasecmp($element, TYPE_TEXT) == 0 || 
                strcasecmp($element, TYPE_SCHEMA) == 0 || 
                strcasecmp($element, TYPE_SEQUENCE) == 0 || 
                strcasecmp($element, TYPE_SELECTOR) == 0 || 
                strcasecmp($element, TYPE_FIELD) == 0 || 
                strcasecmp($element, TYPE_ANNOTATION) == 0 || 
                strcasecmp($element, TYPE_DOCUMENTATION) == 0) {
                return true;
        } else {
            return false;
        }



    /*
    require __DIR__ . '/vendor/autoload.php';
    use Goetas\XML\XSDReader\SchemaReader;

    $reader = new SchemaReader();

    $schema = $reader->readFile("http://localhost/cs4221project/test_file/xsdSample01.xsd");
         //var_dump($schema);
        //  foreach ($schema->getSchemas() as $innerSchema){
        //     echo $innerSchema . "</br></br>";
        // }

    //     foreach ($schema->getElements() as $element){
    //         echo $element . "</br></br>";
    // }
    echo "number of schemas: ". ' ' . count($schema->getSchemas()) . "<br/>";
    echo "number of elements in schema: " . ' ' . count($schema->getElements()) . "<br/>";
    echo "number of types in schema: " . ' ' . count($schema->getTypes()) . "<br/>";
    echo "number of attr in schema: " . ' ' . count($schema->getAttributes()) . "<br/>";
    echo "number of groups in schema: " . ' ' . count($schema->getGroups()) . "<br/>";
    echo "number of attr groups in schema: " . ' ' . count($schema->getAttributeGroups()) . "<br/>";

        //echo "number of attributes in schema: " . ' ' . (string)$schema->findType('', '');

    echo "<br/><br/>";

    foreach ($schema->getElements() as $ele) {
        echo "Name: " . $ele->getName() . ", ";
        $type = (string)$ele->getType();
            //echo "(Test type: " . ' ' . $type . "). ";
            //echo "number of schema in " . $sch->getName() . ": " . count($sch->getSchemas() . "<br/>";
        if ($type == "") {
            echo " Type: nothing detected" . ' ' . "<br/>";

            foreach ($ele->getSchemas()->getSchemas()->getSchemas() as $schEle) {
                foreach($schEle->getElements() as $newEle) {
                    echo "schEle: " . ' ' . $newEle->getName();
                    echo "schEle: " . ' ' . $newEle->getType();
                }
            }
        } elseif ($type == "string") {
            echo "Type: string" . "<br/>";
        } elseif ($type == "positiveInteger") {
            echo "Type: positiveInteger" . "<br/>";
        } else {
            echo "Type: - of the above" . "<br/>";
        }
        echo "<br/>";

        $found = $ele->findType('testComplexType', null);
        print_r($found);

        echo "<br/><br/>";
    }

    function iterateElement($elements) {
        foreach ($schema->getElements() as $ele) {
            echo "Name: " . $ele->getName() . ", ";
            $type = (string)$ele->getType();
            //echo "(Test type: " . ' ' . $type . "). ";
            //echo "number of schema in " . $sch->getName() . ": " . count($sch->getSchemas() . "<br/>";

            if ($type == "") {
                echo " Type: nothing detected" . ' ' . "<br/>";

                foreach ($ele->getElements() as $schEle) {
                    $newElement = iterateElement($elements);    
                    echo "schEle: " . ' ' . $schEle->getName();
                    echo "schEle: " . ' ' . $schEle->getType();
                }
            } elseif ($type == "string") {
                echo "Type: string" . "<br/>";
            } elseif ($type == "positiveInteger") {
                echo "Type: positiveInteger" . "<br/>";
            } else {
                echo "Type: - of the above" . "<br/>";
            }
            echo "<br/>";
        }
    }
    */
        // echo "name of element: " . $schema->getElements()->getName();

        // foreach ($schema->getElements() as $sch){
        //     echo "name of element: " . $sch->getName();

        //     echo "#of elements in schema:" . "</br>";
        //     echo count($sch->getElements()) . "</br>";
        //     echo "name of schema: " . $sch->getName();
        //     foreach($sch->getElements() as $ele) {
        //         echo "$ele: " . $ele . "<br/>";
        //         $eleNodeName = $ele->getSchema()->getElementNode()->getName();
        //         echo "name for the element node: " . $eleNode . "<br/>";

        //         /*echo "ele name: " . ' ' .$ele->getName() . "</br>";
        //         echo "number of attr for ele". "</br>";
        //         echo count($ele->getAttributes()) . "</br>";*/
        //         // echo "</br></br>";
        //     }
        // }

        // echo "+++++++++++++++++++++++++++++";


        /*$doc = new DOMDocument;
        $doc->preserveWhiteSpace = true;
        $doc->load("http://localhost/cs4221project/test_file/xsdSample01.xsd");
        $doc->save('t.xml');
        $xmlfile = file_get_contents("http://localhost/cs4221project/test_file/xsdSample01.xml");
        $parseObj = str_replace($doc->lastChild->prefix.':',"",$xmlfile);
        $ob= simplexml_load_string($parseObj);
        $json  = json_encode($ob);
        $data = json_decode($json, true);
        print_r($data);*/


        // "</br></br></br>";
    ?>